/**
 * Membuat instance dari axios,
 * agar pengguannya bisa tinggal panggil instance axiosnya 
 */
import axios from 'axios'

export default axios.create({
    baseURL: 'http://localhost:3000/'
});